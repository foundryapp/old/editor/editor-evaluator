const ky = require('ky-universal');

const wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

class Logger {
  constructor(app) {
    this.listeners = [];
    this.app = app;
    this.listening = false;
    this.accessToken = '';
    this.isRefreshed = false;
    this.filter = (timestamp) => `"resource.type="cloud_function" AND NOT (resource.labels.function_name="getLogsAccessToken" OR resource.labels.function_name="updateDeploy" OR resource.labels.function_name="createDeploy") resource.labels.region="us-central1" timestamp<="2200-01-01T00:00:00.000000000Z" timestamp<"${timestamp}"`;
    // this.filter = (timestamp) => `resource.type="cloud_function" AND resource.labels.region="us-central1"`;
    this.apiKey = 'AIzaSyBYrvL5je61dZyBSLwZHXQFV4xjB1FZN6o';
    this.timestamp = new Date();
  }

  stop() {
    this.isStreaming = false;
  }

  async start() {
    while (this.isStreaming) {
      // eslint-disable-next-line no-await-in-loop
      const { entries } = await this.getLogs();
      if (entries.length > 0) {
        this.broadcast(entries);
      }
      // eslint-disable-next-line no-await-in-loop
      await wait(1000);
    }
  }

  broadcast(payload) {
    // put filtering or relevant functions and severity here?
    this.listeners.forEach((subscriber) => subscriber(payload));
  }

  subscribe(listener) {
    this.listeners.push(listener);
    this.start();
  }

  unsubscribe(listener) {
    this.listeners = this.listeners.filter((subscriber) => subscriber !== listener);
    if (this.listeners.length === 0) {
      this.stop();
    }
  }

  unsubscribeAll() {
    this.listeners = [];
    this.stop();
  }

  async getAccessToken() {
    const getLogsAccessToken = this.app.functions().httpsCallable('getLogsAccessToken');
    const result = await getLogsAccessToken({});
    const { data } = result;
    this.accessToken = data.accessToken;
    this.isRefreshed = true;
  }

  async getLogs() {
    if (!this.accessToken || !this.isRefreshed) {
      await this.getAccessToken();
    }
    try {
      const requestPayload = {
        pageSize: 100,
        resourceNames: [
          'projects/foundry-editor',
        ],
        filter: this.filter(this.timestamp),
        orderBy: 'timestamp asc',
      };
      this.timestamp = new Date();
      const url = 'https://logging.googleapis.com/v2/entries:list';
      const result = await ky.post(url, {
        // method,
        body: JSON.stringify(requestPayload),
        headers: {
          Authorization: `Bearer ${this.accessToken}`,
          Accept: 'application/json',
          'Content-type': 'application/json',
        },
        searchParams: {
          key: this.apiKey,
        },
      }).json();
      return result;
    } catch (err) {
      console.log(err);
      // throw err;
    }
  }
}

module.exports.Logger = Logger;
