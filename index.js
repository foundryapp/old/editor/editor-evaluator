const generator = require('./generator');
const logging = require('./logging');

module.exports.Generato = generator.Generator;
module.exports.Logger = logging.Logger;
