function makeId(length) {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i += 1) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function getIdInScope(scope) {
  let id;
  for (let i = 0; i < 10000; i += 1) {
    id = makeId(10);
    if (!scope.has(id)) {
      return `id_${id}`;
    }
  }
}

function isValidTriggerAction(trigger) {
  return (trigger.action === 'create') || (trigger.action === 'write') || (trigger.action === 'update') || (trigger.action === 'delete');
}

function isValidTrigger(trigger) {
  if (!trigger) {
    return false;
  }
  const isOnCallTrigger = (trigger.type === 'onCall') && trigger.callName;
  const isFirestoreTrigger = (trigger.type === 'firestore') && trigger.callName && isValidTriggerAction(trigger);
  return isOnCallTrigger || isFirestoreTrigger;
}

function getTypeString(arg) {
  if (typeof arg === 'boolean') {
    return arg;
  } if (typeof arg === 'string') {
    return `'${arg}'`;
  } if (typeof arg === 'object' && arg.constructor === Array) {
    const args = arg.map((m) => getTypeString(m));
    return `[${args.join(', ')}]`;
  } if (typeof arg === 'object') {
    return `JSON.parse('${JSON.stringify(arg)}')`;
  }
  return arg;
}

const STATUS = {
  initializing: 'initializing',
  deploying: 'deploying',
  deployed: 'deployed',
  error: 'error',
};

class Generator {
  constructor(app, id) {
    this.id = id;
    this.app = app;
    this.scope = new Set();
    this.code = '';
    this.trigger = undefined;
    this.doc = this.app.firestore().collection('evalcode').doc(this.id);
    this.listeners = [];
    this.onSnapshot = this.doc.onSnapshot((snapshot) => this.broadcast(snapshot));
  }

  broadcast(snapshot) {
    if (!snapshot.exists) {
      return;
    }
    const data = snapshot.data();
    const { status, trigger } = data;
    const payload = {
      status,
      callName: trigger.callName,
      id: this.id,
    };
    this.listeners.forEach((subscriber) => subscriber(payload));
  }

  localBroadcast(payload) {
    this.listeners.forEach((subscriber) => subscriber(payload));
  }

  subscribe(listener) {
    this.listeners.push(listener);
  }

  unsubscribe(listener) {
    this.listeners = this.listeners.filter((subscriber) => subscriber !== listener);
  }

  unsubscribeAll() {
    this.onSnapshot();
    this.listeners = [];
  }

  getParamsCode(args, stringify = true) {
    const paramsId = `param_${getIdInScope(this.scope)}`;
    let paramsCode = `\nconst ${paramsId} = {`;
    Object.keys(args || []).forEach((key) => {
      const typeString = stringify ? getTypeString(args[key]) : args[key];
      paramsCode += `\n  ${key}: ${typeString},`;
    });
    paramsCode += '\n};';
    return { paramsId, paramsCode };
  }

  assembleCode(id, blockName, dataArgs, args) {
    const { paramsId: dataArgsId, paramsCode: dataArgsCode } = this.getParamsCode(dataArgs, false);
    const { paramsId: argsId, paramsCode: argsCode } = this.getParamsCode(args);
    const code = blockName ? `\nconst ${id} = ${blockName}(${dataArgsId}, ${argsId});` : '';
    return dataArgsCode + argsCode + code;
  }

  assembleCloudFunction(id, args) {
    const type = 'cloudFunction';
    this.trigger = args.trigger;
    const code = this.assembleCode(id, `${type}`, { data: 'input' }, { ...args.trigger });
    const comment = `\n/* START BLOCK ${type} >> ${id} << */\n`;
    this.code += `${comment}\n${code}\n/* END BLOCK */\n\n`;
  }

  assembleStandardBlock(id, dataArgs, args, type, isAsync = true) {
    const awaitCode = isAsync ? 'await ' : '';
    const code = this.assembleCode(id, `${awaitCode}${type}`, dataArgs, args);
    const comment = `\n/* START BLOCK ${type} >> ${id} << */\n`;
    this.code += `${comment}\n${code}\n/* END BLOCK */\n\n`;
  }

  assembleOnCallReturn(dataArgs) {
    if (this.trigger.type !== 'onCall') {
      throw new Error('The onCall function for this onCall return does not exist');
    }
    const type = 'onCallReturn';
    const { paramsId: dataArgsId, paramsCode: dataArgsCode } = this.getParamsCode(dataArgs, false);
    const code = `
    \n${dataArgsCode}
    \nreturn ${dataArgsId};\n`;
    const comment = `\n/* START BLOCK ${type} >> ${this.trigger.callName} << */\n`;
    this.code += `${comment}\n${code}\n/* END BLOCK */\n\n`;
  }

  assembleFirestoreGet(id, dataArgs, args) {
    const type = 'firestoreGet';
    this.assembleStandardBlock(id, dataArgs, args, type);
  }

  assembleFirestoreSetDocument(id, dataArgs, args) {
    const type = 'firestoreSetDocument';
    this.assembleStandardBlock(id, dataArgs, args, type);
  }

  assembleFirestoreDelete(id, dataArgs, args) {
    const type = 'firestoreDelete';
    this.assembleStandardBlock(id, dataArgs, args, type);
  }

  assembleAlgoliaCreate(id, dataArgs, args) {
    const type = 'algoliaCreate';
    this.assembleStandardBlock(id, dataArgs, args, type);
  }

  assembleAlgoliaDelete(id, dataArgs, args) {
    const type = 'algoliaDelete';
    this.assembleStandardBlock(id, dataArgs, args, type);
  }

  assembleAlgoliaSearch(id, dataArgs, args) {
    const type = 'algoliaSearch';
    this.assembleStandardBlock(id, dataArgs, args, type);
  }

  assembleCustomCode(id, dataArgs, args) {
    if (this.isInScope(id)) {
      throw new Error('non unique id');
    }
    const type = 'customCode';
    const comment = `\n/* START BLOCK ${type} >> ${id} << */\n`;
    const { paramsId: dataArgsId, paramsCode: dataArgsCode } = this.getParamsCode(dataArgs, false);
    const code = `
      const ${id} = await (async (data) => {
        ${args.code}
      })(${dataArgsId});
    `;
    this.code += `${comment}\n${dataArgsCode}\n${code}\n/* END BLOCK */\n\n`;
  }

  async deploy() {
    const code = this.getCode();
    const trigger = this.getTrigger();
    if (!isValidTrigger(trigger)) {
      console.error('invalid trigger');
      this.localBroadcast({ status: STATUS.error });
      // throw new Error('invalid function trigger (no callName or no type or bad action)');
    }
    const doc = await this.doc.get();
    const functionObj = {
      code,
      trigger,
      id: this.id,
      status: STATUS.initializing,
      timestamp: new Date(),
    };
    if (doc.status !== STATUS.deploying && doc.status !== STATUS.initializing) {
      return this.doc.set(functionObj);
    }
  }

  isInScope(id) {
    return this.scope.has(id);
  }

  getCode() {
    return this.code;
  }

  getTrigger() {
    return this.trigger;
  }

  clear() {
    this.scope = new Set();
    this.trigger = undefined;
    this.code = '';
  }
}

module.exports.Generator = Generator;
module.exports.STATUS = STATUS;
